<?php include 'php/functions.php' ; ?>

<!DOCTYPE html>

<html lang="en">
<head>

	<title>Phrasebook Wiki</title>
	<!-- Icon found at http://www.iconarchive.com/show/multipurpose-alphabet-icons-by-hydrattz/Letter-W-black-icon.html -->
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />

	<link href="css/index.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Cinzel' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Gilda-Display' rel='stylesheet' type='text/css'>
	
	<script src="jquery/jquery-1.11.0.min.js"></script>
	<script src="jsfunctions/functions.js"></script>

</head>

<body>

	<div id="top">
		<header>
			<div id="wiki">
				<p><a href="index.php">Phrasebook Wiki  </a></p>
			</div>
		</header>
		<nav>
			<div id="links">
				<ul>
					<li><a href="index.php" title="Search the prasebook" style="text-decoration: underline">Search</a></li>
					<li>  |  </li>
					<li><a href="add.php" title="Contribute an entry to the phrasebook">Contribute</a></li>
					<li>  |  </li>
					<li><a href="request.php" title="Request a phrase's translation">Request</a></li>
					<li>  |  </li>
					<li><a href="missing.php" title="Add a missing topic or language">Missing Topics &amp; Languages</a></li>
				</ul>
			</div>
		</nav>
	</div>
	
	<div id="main">
		<form name="search" action="index.php" method="POST" id="form">
			<div id="inputBox">
					<div class="languageLabel">
						<p><label for="source">From:</label></p>
						<?php getLanguages('source', 'form'); ?>
						<p><label for="topic"> Topic: </label></p>
						<?php getTopics(); ?>
					</div>
					
					<div id="input">
						<textarea maxlength="150" rows="10" cols="52" style="resize:none" id="textarea" name="input" title='Phrase input text area. No numbers or special characters allowed.'><?php 
								echo $_POST['input'];
						?></textarea>
					</div>
					
					<input type="submit" value="Search" id="submit"/>
				
				
				<div id="inputNotify">
					<p id="inputMessage"></p>
				</div>
			</div>
		</form>
		
			<div id="outputBox">
				<div class="languageLabel">
					<p><label for="target">To:</label></p>
					<?php getLanguages("target", "form"); ?>
				</div>

				<div id="output">
					<p id="outputMessage">
						<?php
							if (isset($_POST['source']) && isset($_POST['target']) && isset($_POST['topic']) && isset($_POST['input'])) {
								search($_POST['source'], $_POST['target'] , $_POST['topic'], $_POST['input']) ;
							}
						?>
					</p>
				</div>

				<div>
					<div id="button"></div>
				</div>
				
				<div id="outputNotify"></div>
				
			</div>
		
	</div>

</body>
</html>