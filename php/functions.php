<?php

	// The function that runs at the search page
	function search($source, $target, $topic, $input) {

		// Validate input
		$input = trim($input);
		if (checkIO($input)) { return ; }
		if (checkLang($source, $target)) { return ; }
		if (isSpam($input)) { return ; }

		require_once('./db/pdo_construct.php');
		try {
			$db = new myPDO();
			
			// Search the database for results
			// If topic is set to 'Any', else take "topic" into account
			if ($topic==0) {
				$sql = "SELECT entry 
						FROM entries 
						WHERE language=:target
					      AND entryNo = (SELECT *
										 FROM (SELECT entryNo
											   FROM entries
											   WHERE entry=:input AND language=:source)  AS A
							        	 WHERE entryNo IN 
										 (SELECT entryNo FROM entries WHERE language=:target)
										)";
			} else {
				$sql = "SELECT entry 
						FROM entries 
						WHERE language=:target 
					      AND topicID=:topic 
					      AND entryNo = (SELECT *
										 FROM (SELECT entryNo
											   FROM entries
											   WHERE entry=:input AND language=:source)  AS A
							        	 WHERE entryNo IN 
										 (SELECT entryNo FROM entries WHERE language=:target)
										)";
			}
			
			// Execute pdo statement
			$statement = $db->prepare($sql);
			$statement->bindParam(':source', $source, PDO::PARAM_STR);
			$statement->bindParam(':target', $target, PDO::PARAM_STR);
			$statement->bindParam(':topic', $topic, PDO::PARAM_STR);
			$statement->bindParam(':input', $input, PDO::PARAM_STR);
			$statement->execute();
			
			// Return the stored translation. If it doesn't exist, notify the user.
			if ($res = $statement->fetch(PDO::FETCH_ASSOC)) {
				echo $res['entry'] ;
			} else {
				echo "No translation was found."."<br />"."<br />" ;
				if (similar($input, $source) != '') {
					echo similar($input, $source)."</p>";		// possibly return suggestion
				}
				else if ($topic != 0) {
					echo "Tip: Try searching without choosing a topic.";		// give the user a suggestion
				}
			}

			$db = null ;
		}
		catch (PDOException $e) { echo $e->getMessage() ; }
	}
	
	// The function that runs on the contribute page
	function add($source, $target, $topic, $input, $translation) {

		// Validate input
		$input = trim($input);
		$translation = trim($translation);

		if (checkIO($input)) { return ; }
		if (checkIO($translation)) { return ; }
		if (checkLang($source, $target)) { return ; }
		if (isSpam($input)) { return ; }
		if (isSpam($translation)) { return ; }

		require_once('./db/pdo_construct.php');
		try {
			$db = new myPDO();
			
			// Look if the input phrase already exists
			$sql1 = "SELECT * FROM (SELECT exists (SELECT * FROM entries WHERE language=:source AND entry=:input AND topicID=:topic)) AS T1 ";
			$statement1 = $db->prepare($sql1);
			$statement1->bindParam(':source', $source, PDO::PARAM_STR);
			$statement1->bindParam(':input', $input, PDO::PARAM_STR);
			$statement1->bindParam(':topic', $topic, PDO::PARAM_STR);
			$statement1->execute();
			$res1 = $statement1->fetchColumn(0);

			// If the input phrase exists, look if a translation of it exists in the target language
			$res11 == 0 ;
			if ($res1 == 1) {$sql11 = "SELECT entry FROM entries WHERE language=:target AND topicID=:topic AND entryNo = (SELECT * FROM (SELECT entryNo FROM entries WHERE entry=:input AND language=:source) 
										AS A WHERE entryNo IN (SELECT entryNo FROM entries WHERE language=:target))";
				$statement11 = $db->prepare($sql11);
				$statement11->bindParam(':source', $source, PDO::PARAM_STR);
				$statement11->bindParam(':input', $input, PDO::PARAM_STR);
				$statement11->bindParam(':topic', $topic, PDO::PARAM_STR);
				$statement11->bindParam(':target', $target, PDO::PARAM_STR);
				$statement11->execute();

				if ($statement11->fetch(PDO::FETCH_ASSOC)) { $res11 = 1 ;}
			}

			// Look if the translation already exists
			$sql2 = "SELECT * FROM (SELECT exists (SELECT * FROM entries WHERE language=:target AND entry=:translation AND topicID=:topic)) AS T2 ";
			$statement2 = $db->prepare($sql2);
			$statement2->bindParam(':target', $target, PDO::PARAM_STR);
			$statement2->bindParam(':translation', $translation, PDO::PARAM_STR);
			$statement2->bindParam(':topic', $topic, PDO::PARAM_STR);
			$statement2->execute();
			$res2 = $statement2->fetchColumn(0);

			// If the translation phrase exists, look if a translation of it exists in the source language
			$res22 = 0 ;
			if ($res2 == 1) {
				$sql22 = "SELECT entry FROM entries WHERE language=:source AND topicID=:topic AND entryNo = (SELECT * FROM (SELECT entryNo FROM entries WHERE entry=:translation AND language=:target)  AS A
							WHERE entryNo IN (SELECT entryNo FROM entries WHERE language=:source))";
				$statement22 = $db->prepare($sql22);
				$statement22->bindParam(':source', $source, PDO::PARAM_STR);
				$statement22->bindParam(':translation', $translation, PDO::PARAM_STR);
				$statement22->bindParam(':topic', $topic, PDO::PARAM_STR);
				$statement22->bindParam(':target', $target, PDO::PARAM_STR);
				$statement22->execute();

				if ($row22 = $statement22->fetch(PDO::FETCH_ASSOC)) { $res22 = 1 ;}
			}

			// This query looks if someone has already requested the input phrase
			$query1 = "SELECT entryNo FROM entries WHERE language=:target AND entry='' AND topicID=:topic AND entryNo = (SELECT * FROM (SELECT entryNo from entries WHERE language=:source AND entry=:input) AS A 
							WHERE entryNo IN (SELECT entryNo FROM entries where language=:target))";
			$st1 = $db->prepare($query1);
			$st1->bindParam(':source', $source, PDO::PARAM_STR);
			$st1->bindParam(':target', $target, PDO::PARAM_STR);
			$st1->bindParam(':topic', $topic, PDO::PARAM_STR);
			$st1->bindParam(':input', $input, PDO::PARAM_STR);
			$st1->execute();
			$q1 = $st1->fetchColumn(0);

			// This query looks if someone has already requested the translation phrase
			$query2 = "SELECT entryNo FROM entries WHERE language=:source AND entry='' AND topicID=:topic AND
						entryNo = (SELECT * FROM (SELECT entryNo from entries WHERE language=:target AND entry=:translation) AS A
							WHERE entryNo IN (SELECT entryNo FROM entries WHERE language=:source))";
			$st2 = $db->prepare($query2);
			$st2->bindParam(':source', $source, PDO::PARAM_STR);
			$st2->bindParam(':target', $target, PDO::PARAM_STR);
			$st2->bindParam(':topic', $topic, PDO::PARAM_STR);
			$st2->bindParam(':translation', $translation, PDO::PARAM_STR);
			$st2->execute();
			$q2 = $st2->fetchColumn(0);
			
			// If none of the two fields exist, add them to the phrasebook
			if (($res1 == 0 && $res2==0) || ($res11 == 0 & $res22 == 0)) {
				$sql3 = "INSERT INTO entries (entryID, entryNo, topicID, language, entry)
						 VALUES ((SELECT max(entryID)+1 FROM entries AS E1), (SELECT max(entryNo)+1 FROM entries AS E4), :topic, :source, :input),
						        ((SELECT max(entryID)+1 FROM entries AS E2), (SELECT max(entryNo) FROM entries AS E5), :topic, :target, :translation)   ";
						 
				$statement3 = $db->prepare($sql3);
				$statement3->bindParam(':source', $source, PDO::PARAM_STR);
				$statement3->bindParam(':target', $target, PDO::PARAM_STR);
				$statement3->bindParam(':topic', $topic, PDO::PARAM_STR);
				$statement3->bindParam(':input', $input, PDO::PARAM_STR);
				$statement3->bindParam(':translation', $translation, PDO::PARAM_STR);
				$statement3->execute();
				
				echo "Your translation has been successfully added to the phrasebook.";
			}

			// If this was a requested phrase, add only the translation
			else if ($q1>0) {
				$sql3 = "UPDATE entries SET entry=:translation WHERE language=:target AND entry='' AND topicID=:topic AND
						 entryNo = (SELECT * FROM (SELECT entryNo FROM entries WHERE language=:source AND entry=:input) as E WHERE entryNo IN 
						 	                          (SELECT entryNo FROM (SELECT * FROM entries) AS A WHERE language=:target))  ";
				$statement3 = $db->prepare($sql3);
				$statement3->bindParam(':source', $source, PDO::PARAM_STR);
				$statement3->bindParam(':target', $target, PDO::PARAM_STR);
				$statement3->bindParam(':topic', $topic, PDO::PARAM_STR);
				$statement3->bindParam(':input', $input, PDO::PARAM_STR);
				$statement3->bindParam(':translation', $translation, PDO::PARAM_STR);
				$statement3->execute();
				
				echo "Your translation has been successfully added to the phrasebook.";
			}
			
			// If someone requested the translation, add only the input phrase
			else if ($q2>0) {
				$sql3 = "UPDATE entries SET entry=:input WHERE language=:source AND entry='' AND topicID=:topic AND entryNo = (SELECT * FROM
						 	(SELECT entryNo FROM entries WHERE language=:target and entry=:translation) as E WHERE entryNo IN (SELECT entryNo FROM entries WHERE language=:source))  ";
				$statement3 = $db->prepare($sql3);
				$statement3->bindParam(':source', $source, PDO::PARAM_STR);
				$statement3->bindParam(':target', $target, PDO::PARAM_STR);
				$statement3->bindParam(':topic', $topic, PDO::PARAM_STR);
				$statement3->bindParam(':input', $input, PDO::PARAM_STR);
				$statement3->bindParam(':translation', $translation, PDO::PARAM_STR);
				$statement3->execute();
				
				echo "Your translation has been successfully added to the phrasebook.";
			}

			else if ($res22 == 1) {
				$sql4 = "SELECT entry FROM entries WHERE language=:source AND entryNo = 
						(SELECT * FROM (SELECT entryNo FROM entries WHERE language=:target AND entry=:translation) as A WHERE entryNo in (SELECT entryNo FROM entries WHERE language=:source))";
				$statement4 = $db->prepare($sql4);
				$statement4->bindParam(':source', $source, PDO::PARAM_STR);
				$statement4->bindParam(':translation', $translation, PDO::PARAM_STR);
				$statement4->bindParam(':target', $target, PDO::PARAM_STR);
				$statement4->execute();

				$res4 = $statement4->fetch(PDO::FETCH_ASSOC);
				$var = $res4['entry'];
				echo "A translation for the phrase '".$translation."' already exists: ".$var.".";
				echo "You can <a href='edit.php?input=$var&translation=$input&source=$target&target=$source&topic=$topic' style='color:blue'>edit</a> it.";
			}

			// If a translation has already been given, notify the user
			else if (($res1==1 && $res2==1) || ($res11==1 && $res2==0)) {
				$sql3 = "SELECT entry FROM entries WHERE language=:target AND entryNo = 
						 (SELECT * FROM (SELECT entryNo FROM entries WHERE language=:source AND entry=:input) as A WHERE entryNo IN (SELECT entryNo FROM entries WHERE language=:target) )";
				$statement3 = $db->prepare($sql3);
				$statement3->bindParam(':source', $source, PDO::PARAM_STR);
				$statement3->bindParam(':input', $input, PDO::PARAM_STR);
				$statement3->bindParam(':target', $target, PDO::PARAM_STR);
				$statement3->execute();

				$res3 = $statement3->fetch(PDO::FETCH_ASSOC);
				$var = $res3['entry'];
				echo "A translation for this phrase already exists: ".$var.".<br/>";
				echo "You can <a href='edit.php?input=$input&translation=$var&source=$source&target=$target&topic=$topic' style='color:blue'>edit</a> it.";
			}

			$db = null ;
		}
		catch (PDOException $e) { echo $e->getMessage() ; }
	}

	// The functions that runs on the edit page
	function edit($source, $target, $topic, $input, $translation) {

		// Validate input
		$input = trim($input);
		$translation = trim($translation);

		if (checkIO($input)) { return ; }
		if (checkIO($translation)) { return ; }
		if (checkLang($source, $target)) { return ; }
		if (isSpam($input)) { return ; }
		if (isSpam($translation)) { return ; }

		require_once('./db/pdo_construct.php');
		try {
			$db = new myPDO();

			$sql = "UPDATE entries SET entry=:translation WHERE language=:target AND topicID=:topic AND entryNo = (SELECT * FROM (SELECT entryNo FROM entries
				    WHERE entry=:input AND language=:source) AS A WHERE entryNo IN (SELECT entryNo FROM (SELECT * FROM entries) as B WHERE language=:target))";
			
			$statement = $db->prepare($sql);
			$statement->bindParam(':source', $source, PDO::PARAM_STR);
			$statement->bindParam(':target', $target, PDO::PARAM_STR);
			$statement->bindParam(':topic', $topic, PDO::PARAM_STR);
			$statement->bindParam(':input', $input, PDO::PARAM_STR);
			$statement->bindParam(':translation', $translation, PDO::PARAM_STR);
			$statement->execute();

			echo "You have successfully edited this entry." ;

			$db = null ;
		}
		catch (PDOException $e) { echo $e->getMessage() ; }
	}

	// The function that runs on the request page
	function request($source, $target, $topic, $input) {

		// Validate input
		$input = trim($input);

		if (checkIO($input)) { return ; }
		if (checkLang($source, $target)) { return ; }
		if (isSpam($input)) { return ; }

		require_once('./db/pdo_construct.php');
		try {
			$db = new myPDO();

			$sql = "SELECT entry FROM entries WHERE language=:target AND topicID=:topic AND entryNo = (SELECT * FROM (SELECT entryNo FROM entries WHERE entry=:input AND language=:source)  AS A
					WHERE entryNo IN (SELECT entryNo FROM entries WHERE language=:target))  ";
			
			$statement = $db->prepare($sql);
			$statement->bindParam(':source', $source, PDO::PARAM_STR);
			$statement->bindParam(':target', $target, PDO::PARAM_STR);
			$statement->bindParam(':topic', $topic, PDO::PARAM_STR);
			$statement->bindParam(':input', $input, PDO::PARAM_STR);
			$statement->execute();
			$res = $statement->fetchColumn(0);

			if (strlen($res)>0) {
				echo "This phrase has already been translated:  ".$res ;
			} else {
				$sql2 = "INSERT INTO entries 
						 VALUES ((SELECT max(entryID)+1 FROM entries AS E1), (SELECT max(entryNo)+1 FROM entries AS E4), :topic, :source, :input),
						        ((SELECT max(entryID)+1 FROM entries AS E2), (SELECT max(entryNo) FROM entries AS E5), :topic, :target, '')          ";
			
				$statement2 = $db->prepare($sql2);
				$statement2->bindParam(':source', $source, PDO::PARAM_STR);
				$statement2->bindParam(':target', $target, PDO::PARAM_STR);
				$statement2->bindParam(':topic', $topic, PDO::PARAM_STR);
				$statement2->bindParam(':input', $input, PDO::PARAM_STR);
				$statement2->execute();

				echo "You have successfully requested a translation for this phrase." ;
			}

			$db = null ;
		}
		catch (PDOException $e) { echo $e->getMessage() ; }
	}

	// Populate the language drop-down menu
	function getLanguages ($val, $form) {
		require_once('./db/pdo_construct.php');
		try {
			$db = new myPDO();

			$sql = "SELECT * FROM languages ORDER BY language";
			
			$statement = $db->prepare($sql);
			$statement->execute();
			
			$menu = "<select class='comboBox' name='$val' id='$val' form='$form' title='language drop-down menu'>";
			$post = $_POST[$val];
			$get = $_GET[$val];

			while ($option = $statement->fetch(PDO::FETCH_ASSOC)) {
				$code = "{$option['code']}";
				$lang = "{$option['language']}";
				$bool = false ;

				if ($post==$code) { $bool = true ; }
				else if ($get==$code) { $bool = true ; }

				if ($bool) {
					$menu .= "<option value=$code selected>$lang</option>";
				}
				else {
					$menu .= "<option value=$code>$lang</option>";
				}
			}
			$menu .= "</select>" ;
			
			echo $menu;

			$db = null ;
		}
		catch (PDOException $e) { echo $e->getMessage() ; }
	}

	// Populate the topics drop-down menu
	function getTopics () {
		require_once('./db/pdo_construct.php');
		try {
			$db = new myPDO();

			$sql = "SELECT * FROM topics ORDER BY topic";
			
			$statement = $db->prepare($sql);
			$statement->execute();
			
			$menu = "<select class='comboBox' name='topic' id='topic' title='topics drop-down menu'>";
			$post = $_POST['topic'];
			$get = $_GET['topic'];

			while ($option = $statement->fetch(PDO::FETCH_ASSOC)) {
				$id = "{$option['topicID']}";
				$topic = "{$option['topic']}";
				$bool = false ;

				if ($post==$id) { $bool = true ; }
				else if ($get==$id) { $bool = true ; }

				if ($bool) {
					$menu .= "<option value=$id selected>$topic</option>";
				}
				else {
					$menu .= "<option value=$id>$topic</option>";
				}
			}
			$menu .= "</select>" ;
			
			echo $menu;

			$db = null ;
		}
		catch (PDOException $e) { echo $e->getMessage() ; }
	}

	// Add a missing topic to the database
	function addTopic ($topic) {
		$input = trim($topic);
		if (checkIO($topic)) { return ; }
		if (isSpam($input)) { return ; }

		require_once('./db/pdo_construct.php');
		try {
			$db = new myPDO();

			$sql1 = "SELECT count(topic) FROM topics WHERE topic=:topic";
			
			$statement1 = $db->prepare($sql1);
			$statement1->bindParam(':topic', $topic, PDO::PARAM_STR);
			$statement1->execute();

			$res1 = $statement1->fetchColumn(0) ;

			if ($res1 > 0) {
				echo "This topic seems to already exist in our database";
			} else {
				$sql2 = "INSERT INTO topics VALUES ((SELECT max(topicID)+1 FROM topics as A), :topic)";

				$statement2 = $db->prepare($sql2);
				$statement2->bindParam(':topic', $topic, PDO::PARAM_STR);
				$statement2->execute();

				echo "The topic ".$topic." has been successfully added to the phrasebook.";
			}

			$db = null ;
		}
		catch (PDOException $e) { echo $e->getMessage() ; }
	}

	// Add a missing language to the database
	function addLanguage ($code, $language) {

		// Validate input
		$code = trim($code);
		$language = trim($language);

		if (checkIO($code)) { return ; }
		if (checkIO($language)) { return ; }
		if (isSpam($code)) { return ; }
		if (isSpam($language)) { return ; }

		require_once('./db/pdo_construct.php');
		try {
			$db = new myPDO();

			$sql1 = "SELECT count(code) as A, count(language) as B FROM languages WHERE code=:code OR language=:language";
			
			$statement1 = $db->prepare($sql1);
			$statement1->bindParam(':code', $code, PDO::PARAM_STR);
			$statement1->bindParam(':language', $language, PDO::PARAM_STR);
			$statement1->execute();

			$res1 = $statement1->fetch(PDO::FETCH_ASSOC);

			if ($res1['A'] > 0) {
				echo "This code is already in use.";
			} else if ($res1['B'] > 0) {
				echo "This language is already being used by this wiki.";
			} else {
				$sql2 = "INSERT INTO languages VALUES (:code, :language)";

				$statement2 = $db->prepare($sql2);
				$statement2->bindParam(':code', $code, PDO::PARAM_STR);
				$statement2->bindParam(':language', $language, PDO::PARAM_STR);
				$statement2->execute();

				echo "The language ".$language." has been successfully added to the phrasebook.";
			}

			$db = null ;
		}
		catch (PDOException $e) { echo $e->getMessage() ; }
	}

	// Find strings similar to the given string
	function similar($string, $language) {
		require_once('./db/pdo_construct.php');
		try {
			$db = new myPDO();

			$sql1 = "SELECT entry FROM entries WHERE language=:language";
			
			$statement1 = $db->prepare($sql1);
			$statement1->bindParam(':language', $language, PDO::PARAM_STR);
			$statement1->execute();

			$lev = 100 ;
			$str = '' ;
			while ($row = $statement1->fetch(PDO::FETCH_ASSOC)) {
				$entry = $row['entry'];
				$var = levenshtein($string, $entry);
				if ($var < $lev) { 
					$lev = $var ;
					$str = $entry ;
				}
			}

			if ($lev<7 && $string!=$str) {
				echo "\r\n";
				echo "Did you mean ".$str."?";
			}

			$db = null ;
		}
		catch (PDOException $e) { echo $e->getMessage() ; }
	}

	// Check if the string has special characters in it
	function hasSymbol($string) {
		$symbols = "<>@#$%^&*()_+[]{}|\"\\/~`=" ;
		$l = strlen($symbols);

		for ($i=0 ; $i<$l ; $i++) {
			if (strpos($string, $symbols[$i]) !== false) {
				return true;
			}
		}
		return false;
	}

	// Check if the string has numbers in it
	function hasNumber($string) {
		$numbers = "0123456789" ;
		$l = strlen($numbers);

		for ($i=0 ; $i<$l ; $i++) {
			if (strpos($string, $numbers[$i]) !== false) {
				return true;
			}
		}
		return false;
	}

	// Validate input function
	function checkIO ($string) {
		if (hasSymbol($string)) { echo "There is an invalid character in your phrase."; return true ; }
		if (hasNumber($string)) { echo "Numbers are invalid input."; return true ; }
		if ($string == '') { echo "Please provide a phrase." ; return true ; }

		return false;
	}

	// Checks that the source and target language are not the same
	function checkLang ($str1, $str2) {
		if ($str1 == $str2) {
			echo "Please choose a different language for translation.";
			return true ;
		}
		return false ;
	}

	// Detects if a given input is spam or not
	function isSpam($string) {
		require_once('b8/b8.php');
		$config_b8 = array(	'storage' => 'mysql' );
		$config_storage = array('database' => 'ib621db', 'table_name' => 'b8_wordlist', 'host' => 'mysql-student.cs.york.ac.uk', 'user' => 'ib621', 'pass' => 'fencerrust');
		$config_lexer = array('old_get_html' => FALSE, 'get_html' => TRUE);
		$config_degenerator = array('multibyte' => TRUE);

		try {
			$b8 = new b8($config_b8, $config_storage, $config_lexer, $config_degenerator);
			$res = $b8->classify($string);
			if ($res>0.9) { echo "The text you are trying to enter has been identified as suspicious. </br> Please contact the system administrator for help."; return true ; }
			else { return false ; }
		}
		catch(Exception $e) {echo "problem"; exit(); }	
	}
?>

