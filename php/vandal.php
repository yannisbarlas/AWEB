<?php

// This is the function that runs on clicking the flag as inappropriate button

require_once('db/pdo_construct.php');
try {
	$db = new myPDO();
	
	$text = $_POST['text'];
	$lang = $_POST['target'];

	$sql = "INSERT INTO vandalism VALUES ((SELECT entryNo FROM entries WHERE entry=:text and language=:lang), :text)";
	
	$statement = $db->prepare($sql);
	$statement->bindParam(':text', $text, PDO::PARAM_STR);
	$statement->bindParam(':lang', $lang, PDO::PARAM_STR);
	$statement->execute();
	
	// Return the stored translation. If it doesn't exist, notify the user.
	if ($res = $statement->fetch(PDO::FETCH_ASSOC)) {
		echo "The translation has been marked as inappropriate";
	}

	$db = null ;
}
catch (PDOException $e) { echo $e->getMessage() ; }


?>