<?php

// This is the function that runs on clicking the mark as spam button

// Configure the parameters of the b8 object
require_once('b8/b8.php');
$config_b8 = array(	'storage' => 'mysql' );
$config_storage = array(
	'database'   => 'ib621db',
	'table_name' => 'b8_wordlist',
	'host'       => 'mysql-student.cs.york.ac.uk',
	'user'       => 'ib621',
	'pass'       => 'fencerrust'
);
$config_lexer = array(
	'old_get_html' => FALSE,
	'get_html'     => TRUE
);
$config_degenerator = array(
	'multibyte' => TRUE
);

try {
	// Create b8 object and label input as spam
	$b8 = new b8($config_b8, $config_storage, $config_lexer, $config_degenerator);
	$b8->learn($_POST['text'], b8::SPAM);
	echo json_encode("The translation has been marked as spam.") ;
}
catch(Exception $e) {
	echo json_encode("problem");
	exit();
}


?>