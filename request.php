<?php include 'php/functions.php' ; ?>

<!DOCTYPE html>

<html lang="en">
<head>

	<title>Phrasebook Wiki</title>
	<!-- Icon found at http://www.iconarchive.com/show/multipurpose-alphabet-icons-by-hydrattz/Letter-W-black-icon.html -->
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />

	<link href="css/index.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Cinzel' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Gilda-Display' rel='stylesheet' type='text/css'>
	
	<script src="jquery/jquery-1.11.0.min.js"></script>
	<script src="jsfunctions/functions.js"></script>

</head>

<body>

	<div id="top">
		<header>
			<div id="wiki">
				<p><a href="index.php">Phrasebook Wiki  </a></p>
			</div>
		</header>
		<nav>
			<div id="links">
				<ul>
					<li><a href="index.php" title="Search the phrasebook">Search</a></li>
					<li>  |  </li>
					<li><a href="add.php" title="Contribute an entry to the phrasebook">Contribute</a></li>
					<li>  |  </li>
					<li><a href="request.php" title="Request a phrase's translation" style="text-decoration: underline">Request</a></li>
					<li>  |  </li>
					<li><a href="missing.php" title="Add a missing topic or language">Missing Topics &amp; Languages</a></li>
				</ul>
			</div>
		</nav>
	</div>
	
	<div id="main">
		<form name="request" action="request.php" method="POST" id="request">
			<div id="requestBox">
				<div class="languageLabel">
					<p><label for="source">From:</label></p>
					<?php getLanguages("source", "request"); ?>
					<p><label for="topic"> Topic: </label></p>
					<?php getTopics(); ?>
				</div>
				
				<div id="input">
					<textarea rows="10" maxlength="150" cols="52" style="resize:none" id="textarea" name="input" title="Request phrase text area. No numbers or special characters allowed."><?php 
							echo $_POST['input'];
							echo $_GET['input'];
					?></textarea>
				</div>

				<div id="requestTargetLanguage">
					<p id="targetLabel"><label for="target">What language would you like this phrase to be translated in?</label></p>
					<?php getLanguages("target", "request"); ?>
				</div>
				
				<div>
					<input type="submit" value="Submit" id="submit3"/>
				</div>
				
				<div id="requestNotify">
					<p id="requestMessage">
						<?php
							if (isset($_POST['source']) && isset($_POST['target']) && isset($_POST['input']) && isset($_POST['topic'])) {
								request($_POST['source'], $_POST['target'], $_POST['topic'], $_POST['input']);
							}
						?>
					</p>
				</div>
			</div>
		
			
		</form>
	</div>

</body>
</html>