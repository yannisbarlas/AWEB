<?php include 'php/functions.php' ; ?>

<!DOCTYPE html>

<html lang="en">

<head>

	<title>Phrasebook Wiki</title>
	<!-- Icon found at http://www.iconarchive.com/show/multipurpose-alphabet-icons-by-hydrattz/Letter-W-black-icon.html -->
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />

	<link href="css/index.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Cinzel' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Gilda-Display' rel='stylesheet' type='text/css'>
	
	<script src="jquery/jquery-1.11.0.min.js"></script>
	<script src="jsfunctions/functions.js"></script>

</head>

<body>

	<div id="top">
		<header>
			<div id="wiki">
				<p><a href="index.php">Phrasebook Wiki  </a></p>
			</div>
		</header>
		<nav>
			<div id="links">
				<ul>
					<li><a href="index.php" title="Search the phrasebook">Search</a></li>
					<li>  |  </li>
					<li><a href="add.php" style="text-decoration: underline" title="Contribute an entry to the phrasebook">Contribute</a></li>
					<li>  |  </li>
					<li><a href="request.php" title="Request a phrase's translation">Request</a></li>
					<li>  |  </li>
					<li><a href="missing.php" title="Add a missing topic or language">Missing Topics &amp; Languages</a></li>
				</ul>
			</div>
		</nav>
	</div>
	
	<div id="clear"></div>

	<div id="main">
		<form name="add" action="add.php" method="POST" id="addForm">
			<div id="inputBox">
				<div class="languageLabel">
					<p><label for="source">From:</label></p>
					<?php getLanguages("source", "addForm"); ?>
					<p><label for="topic"> Topic: </label></p>
					<?php getTopics(); ?>
				</div>
				
				<div id="input">
					<textarea maxlength="150" rows="10" cols="52" style="resize:none" id="textarea" name="input" title="Phrase input text area. No numbers or special characters allowed."><?php 
							if (isset($_POST['input'])) {
								echo $_POST['input'];
							}
							else{
								echo $_GET['input'];
							}
					?></textarea>
				</div>
				
				<div id="inputNotify">
					<p id="inputMessage"></p>
				</div>
			</div>
			
			<div id="outputBox">
				<div class="languageLabel">
					<p><label for="target">To:</label></p>
					<?php getLanguages("target", "addForm"); ?>
				</div>

				<div id="translationTextarea">
					<textarea form='addForm' maxlength="150" rows='10' cols='52' style='resize:none' id='translation' name='translation' title="Translation input text area. No numbers or special characters allowed."><?php echo $_POST['translation'];?></textarea>
				</div>

				<div>
					<input form="addForm" type="submit" value="Submit" id="submit"/>
				</div>
				
				<div id="outputNotify2">
					<p id="outputMessage">
						<?php
							if (isset($_POST['source']) && isset($_POST['target']) && isset($_POST['topic']) && isset($_POST['input']) && isset($_POST['translation'])) {
								add($_POST['source'], $_POST['target'] , $_POST['topic'], $_POST['input'], $_POST['translation']) ;
							}
						?>
					</p>
				</div>
				
			</div>
		</form>
	</div>

</body>




</html>