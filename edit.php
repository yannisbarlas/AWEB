<?php include 'php/functions.php' ; ?>

<!DOCTYPE html>

<html lang="en">

<head>

	<title>Phrasebook Wiki</title>
	<!-- Icon found at http://www.iconarchive.com/show/multipurpose-alphabet-icons-by-hydrattz/Letter-W-black-icon.html -->
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />

	<link href="css/index.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Cinzel' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Gilda-Display' rel='stylesheet' type='text/css'>
	
	<script src="jquery/jquery-1.11.0.min.js"></script>
	<script src="jsfunctions/functions.js"></script>

</head>

<body>

	<div id="top">
		<header>
			<div id="wiki">
				<p><a href="index.php">Phrasebook Wiki  </a></p>
			</div>
		</header>
		<nav>
			<div id="links">
				<ul>
					<li><a href="index.php" title="Search the phrasebook">Search</a></li>
					<li>  |  </li>
					<li><a href="add.php" title="Contribute an entry to the phrasebook">Contribute</a></li>
					<li>  |  </li>
					<li><a href="request.php" title="Request a phrase's translation">Request</a></li>
					<li>  |  </li>
					<li><a href="missing.php" title="Add a missing topic or language">Missing Topics &amp; Languages</a></li>
				</ul>
			</div>
		</nav>
	</div>
	
	<div id="main">
		<form name="edit" action="edit.php" method="POST" id="edit">
			<div id="inputBox">
				<div class="languageLabel">
					<p><label for="source">From:</label></p>
					<?php getLanguages("source", "edit"); ?>
					<p><label for="topic"> Topic: </label></p>
					<?php getTopics(); ?>
				</div>
				
				<div id="givenInput">
					<p id="givenInputText">
						<?php
						if (isset($_POST['input'])) {
							echo $_POST['input'];
						} else {
							echo $_GET['input'];
						}
						?>
					</p>
					<p><input type="hidden" value="<?php if(isset($_POST['input'])) {echo $_POST['input'];} else {echo $_GET['input'];} ?>" name="input" title="Original phrase. No numbers or special characters allowed."/></p>
				</div>
			
				<div id="inputNotify">
					<p id="inputMessage"></p>
				</div>
			</div>
		</form>
		
		<div id="outputBox">
			<div class="languageLabel">
				<p><label for="target">To:</label></p>
				<?php getLanguages("target", "edit"); ?>
			</div>

			<div id="output">
				<textarea form="edit" maxlength="150" rows="10" cols="52" style="resize:none" id="translation" name="translation" title="Translation to be edited. No numbers or special characters allowed."><?php
							if (isset($_POST['translation'])) {
								echo $_POST['translation'];
							} else {
								echo $_GET['translation'];
							}
				?></textarea>
			</div>

			<div>
				<input form="edit" type="submit" value="Submit" id="submit2"/>
			</div>
			
			<div id="outputNotify">
				<p id="outputMessage">
					<div id="editOutput">
					<?php
						if (isset($_POST['source']) && isset($_POST['target']) && isset($_POST['topic']) && isset($_POST['input']) && isset($_POST['translation'])) {
							edit($_POST['source'], $_POST['target'] , $_POST['topic'], $_POST['input'], $_POST['translation']) ;
						}
					?>
					</div>
				</p>
			</div>
			
		</div>
		
	</div>

</body>




</html>