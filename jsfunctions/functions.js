$(document).ready ( function() {

	// Makes sure that input does not contain special characters
	function hasSymbol(str) {
		// Some symbols intentionally left out, as they are used in some languages for punctuation (eg. ! . , ; ?)
		symbols = "<>@#$%^&*()_+[]{}|\"\\/~`=" ;
		l = symbols.length ;
		for (i=0 ; i<l ; i++) { if (str.indexOf(symbols[i]) != -1) { return true; } }
		return false;
	}

	// Makes sure that input does not contain numbers
	function hasNumber(str) {
		numbers = "0123456789" ;
		l = numbers.length ;
		for (i=0 ; i<l ; i++) { if (str.indexOf(numbers[i]) != -1) { return true; } }
		return false;
	}

	// Makes sure that input is not an empty string
	function isEmpty (s) {
		s = $.trim(s);
		if (s == '') { return true ; }
		return false ;
	}

	// Makes sure that source and target language is not the same
	function checkLang(s1, s2) {
		if (s1==s2) { return true ; }
		return false ;
	}
	
	// What happens when the submit button is clicked (search page, contribute page)
	$("#submit").click( function() {
		// Erase previous messages
		$("#inputMessage").replaceWith('<p id="inputMessage"></p>') ;
		$("#outputMessage").replaceWith('<p id="outputMessage"></p>') ;

		// Validate input
		if (hasSymbol($("#textarea").val())) { $("#inputMessage").replaceWith('<p style="color:red;" id="inputMessage">There is an invalid character in your phrase.</p>') ; return false ; }
		if (isEmpty($("#textarea").val())) { $("#inputMessage").replaceWith('<p style="color:red;" id="inputMessage">Please provide a phrase for translation.</p>') ; return false ; }
		if (hasNumber($("#textarea").val())) { $("#inputMessage").replaceWith('<p style="color:red;" id="inputMessage">Numbers are invalid input. Please provide a valid phrase.</p>') ; return false ; }
		if (checkLang ($("#source").val(), $("#target").val())) { $("#inputMessage").replaceWith('<p style="color:red;" id="inputMessage">You need to select a different language for the translation.</p>') ; return false ;}

		// Validate translation input
		if (hasSymbol($("#translation").val())) { $("#outputMessage").replaceWith('<p style="color:red;" id="outputMessage">There is an invalid character in your phrase.</p>') ; return false; }
		if (isEmpty($("#translation").val())) { $("#outputMessage").replaceWith("<p style='color:red;' id='outputMessage'>Please provide a phrase as a translation. Or maybe you want to <a href='request.php?input=" + $("#textarea").val() + "&source=" + $("#source").val() + "&target=" + $("#target").val() + "&topic=" + $("#topic").val() + "'>request</a> a phrase? </p>") ; return false; }
		if (hasNumber($("#translation").val())) { $("#outputMessage").replaceWith('<p style="color:red;" id="outputMessage">Numbers are invalid input. Please provide a valid phrase.</p>') ; return false; }
	});

	// What happens when the submit button is clicked (edit page)
	$("#submit2").click( function() {
		// Erase previous messages
		$("#editOutput").replaceWith('<p id="editOutput"></p>') ;

		// Validate input
		if (hasSymbol($("#translation").val())) { $("#editOutput").replaceWith('<p style="color:red;" id="editOutput">There is an invalid character in your phrase.</p>') ; return false ; }
		if (isEmpty($("#translation").val())) { $("#editOutput").replaceWith("<p style='color:red;' id='editOutput'>Please provide a phrase as a translation. Or maybe you want to <a href='request.php' style='text-decoration:underline;'>request</a> a phrase? </p>") ; return false ; }
		if (hasNumber($("#translation").val())) { $("#editOutput").replaceWith('<p style="color:red;" id="editOutput">Numbers are invalid input. Please provide a valid phrase.</p>') ; return false ; }
		if (checkLang($("#source").val(), $("#target").val())) { $("#editOutput").replaceWith('<p style="color:red;" id="editOutput">You need to select a different language for the translation.</p>') ; return false ; }
	});
	
	// What happens when the submit button is clicked (request page)
	$("#submit3").click( function() {
		// Erase previous messages
		$("#requestMessage").replaceWith('<p id="requestMessage"></p>') ;

		// Validate input
		if (hasSymbol($("#textarea").val())) { $("#requestMessage").replaceWith('<p style="color:red;" id="requestMessage">There is an invalid character in your phrase.</p>') ; return false ; }
		if (isEmpty($("#textarea").val())) { $("#requestMessage").replaceWith("<p style='color:red;' id='requestMessage'>Please provide a phrase.") ; return false ; }
		if (hasNumber($("#textarea").val())) { $("#requestMessage").replaceWith('<p style="color:red;" id="requestMessage">Numbers are invalid input. Please provide a valid phrase.</p>') ; return false ; }
		if (checkLang($("#source").val(), $("#target").val())) { $("#requestMessage").replaceWith('<p style="color:red;" id="requestMessage">You need to select a different language for the translation.</p>') ; return false ; }
	});

	// What happens when the submit button is clicked (missing topic page)
	$("#submit4").click( function() {
		// Erase previous messages
		$("#missingMessage1").replaceWith('<p id="missingMessage1"></p>') ;
		$("#missingMessage2").replaceWith('<p id="missingMessage2"></p>') ;

		topic = $("#textareaMissingTopic").val();

		// Validate input
		if (hasSymbol(topic)) { $("#missingMessage1").replaceWith('<p style="color:red;" id="missingMessage1">There is an invalid character in your topic.</p>') ; return false ; }
		if (isEmpty(topic)) { $("#missingMessage1").replaceWith("<p style='color:red;' id='missingMessage1'>Please provide a topic.</p>") ; return false ; }
		if (hasNumber(topic)) { $("#missingMessage1").replaceWith('<p style="color:red;" id="missingMessage1">Numbers are invalid input. Please provide a valid topic.</p>') ; return false ; }
	});

	// What happens when the submit button is clicked (missing language page)
	$("#submit5").click( function() {
		// Erase previous messages
		$("#missingMessage1").replaceWith('<p id="missingMessage1"></p>') ;
		$("#missingMessage2").replaceWith('<p id="missingMessage2"></p>') ;

		code = $("#textareaMissingCode").val();
		language = $("#textareaMissingLanguage").val();

		// Validate input
		if (hasSymbol(code)) { $("#missingMessage2").replaceWith('<p style="color:red;" id="missingMessage2">There is an invalid character in your code.</p>') ; return false ; }
		if (hasSymbol(language)) { $("#missingMessage2").replaceWith('<p style="color:red;" id="missingMessage2">There is an invalid character in your language.</p>') ; return false ; }
		if (isEmpty(code)) { $("#missingMessage2").replaceWith("<p style='color:red;' id='missingMessage2'>Please provide a code.</p>") ; return false ; }
		if (isEmpty(language)) { $("#missingMessage2").replaceWith("<p style='color:red;' id='missingMessage2'>Please provide a language.</p>") ; return false ; }
		if (hasNumber(code)) { $("#missingMessage2").replaceWith('<p style="color:red;" id="missingMessage2">Numbers are invalid input. Please provide a valid code.</p>') ; return false ; }
		if (hasNumber(language)) { $("#missingMessage2").replaceWith('<p style="color:red;" id="missingMessage2">Numbers are invalid input. Please provide a valid language.</p>') ; return false ; }
	});


	// Add new translation button if none was found
	$(function() { 
		out = $.trim($("#outputMessage").text());
		str = "No translation was found.";

		// If output starts with "No translation was found.", add button
		if (out.indexOf(str)==0) {
			$("#button").replaceWith('<button id="add">Add translation</button>');
			
			// When add new button is clicked, take user to add page with values filled in
			$("#add").click( function() {
				url = "add.php?input=" + encodeURIComponent($("#textarea").val()) + "&source=" + encodeURIComponent($("#source").val()) + "&target=" + encodeURIComponent($("#target").val()) + "&topic=" + encodeURIComponent($("#topic").val()) ;
				location.href = url ;
			});
		}
		else if (out.length > 0) {
			// Show mark as spam and flag as inappropriate buttons
			$("#button").replaceWith('<button type="submit" id="spam">Mark as spam</button><button id="flag">Flag as inappropriate</button>');

			input = $("#input").text();
			target = $("#target").val();
			source = $("#source").val();
			topic = $("#topic").val();

			// If a translation was found, offer the user the chance to edit it
			$("#outputNotify").replaceWith('<div id="outputNotify"><p>Do you disagree with the translation?<br/>You can <a href="edit.php?input=' + input + '&translation=' + out + '&source=' + source + '&target=' + target + '&topic=' + topic + '" style="color:blue;text-decoration:underline;">edit</a> it.</p></div>') ;
			
			// Ajax for spam and inappropriate buttons
			$('#spam').click( function() {
				$.ajax({
					type: "POST",
					url: "./php/spam.php",
					data: { text: out }
				})
			    .done(function( msg ) {
			      	$("#outputNotify").replaceWith('<div id="outputNotify"><p>The translation has been marked as spam.</p></div>')
			    });
			});

			$('#flag').click( function() {
				$.ajax({
					type: "POST",
					url: "./php/vandal.php",
					data: { text: out , target: target}
				})
			    .done(function( msg ) {
			      	alert( msg + "has been added");
			    });
			});
		}
	});

});


