<?php include 'php/functions.php' ; ?>

<!DOCTYPE html>

<html lang="en">
<head>

	<title>Phrasebook Wiki</title>
	<!-- Icon found at http://www.iconarchive.com/show/multipurpose-alphabet-icons-by-hydrattz/Letter-W-black-icon.html -->
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />

	<link href="css/index.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Cinzel' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Gilda-Display' rel='stylesheet' type='text/css'>
	
	<script src="jquery/jquery-1.11.0.min.js"></script>
	<script src="jsfunctions/functions.js"></script>

</head>

<body>

	<div id="top">
		<header>
			<div id="wiki">
				<p><a href="index.php">Phrasebook Wiki  </a></p>
			</div>
		</header>
		<nav>
			<div id="links">
				<ul>
					<li><a href="index.php" title="Search the phrasebook">Search</a></li>
					<li>  |  </li>
					<li><a href="add.php" title="Contribute an entry to the phrasebook">Contribute</a></li>
					<li>  |  </li>
					<li><a href="request.php" title="Request a phrase's translation">Request</a></li>
					<li>  |  </li>
					<li><a href="missing.php" title="Add a missing topic or language" style="text-decoration: underline">Missing Topics &amp; Languages</a></li>
				</ul>
			</div>
		</nav>
	</div>
	
	<div id="main">
		<form name="misstop" action="missing.php" method="POST" id="misstop">
			<div>
				<div id="missingBox1">
					<p>Do you think we are missing a topic?</p>
					<p><label for="textareaMissingTopic">Please add the topic in the text area below and click the submit button.</label></p>
					<div id="topicArea">
						<textarea name="newTopic" maxlength="100" cols="50" rows="1" style="resize:none" id="textareaMissingTopic" title="New topic input text area. No numbers or special characters allowed."></textarea>
					</div>
					<input form="misstop" type="submit" value="Add" id="submit4">
				</div>

				<div class="missingNotify">
					<p id="missingMessage1">
						<?php
							if (isset($_POST['newTopic'])) {
								addTopic($_POST['newTopic']);
							}
						?>
					</p>
				</div>
			</div>
		</form>

		<form name="misslang" action="missing.php" method="POST" id="misslang">
			<div>
				<div id="missingBox2">
					<p>Do you think we are missing a language?</p>
					<p>Please add the language's <a href="http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes" style="color:blue;text-decoration:underline;" title="ISO language codes in Wikipedia"><abbr title="International Organization for Standardization">ISO</abbr> code</a> and its name in the two text areas below and click the submit button.</p>
					<div id="codeArea">
						<div id="codeLabel"><label for="textareaMissingCode"><abbr title="International Organization for Standardization">ISO</abbr> Code</label></div>
						<textarea name="code" maxlength="2" style="resize:none" rows="1" id="textareaMissingCode" title="Two letter code for language."></textarea>
					</div>
					<div id="languageArea">
						<div id="languageLabel"><label for="textareaMissingLanguage">Language</label></div>
						<textarea name="language" maxlength="35" cols="50" rows="1" style="resize:none" id="textareaMissingLanguage" title="Missing language text area. No numbers or special characters allowed."></textarea>
					</div>
					<input form="misslang" type="submit" value="Add" id="submit5"/>
				</div>

				<div class="missingNotify">
					<p id="missingMessage2">
						<?php
							if (isset($_POST['code']) && isset($_POST['language'])) {
								addLanguage($_POST['code'], $_POST['language']);
							}
						?>
					</p>
				</div>
			</div>
		</form>

		
			
		
			
		
	</div>

</body>
</html>