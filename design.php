<!DOCTYPE html>

<html lang='en'>
<head>
	<title>Assessment Report</title>
	<link href="css/design.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Alike' rel='stylesheet' type='text/css'>
</head>

<body>
	<div id="main">
		<h1>Assessment Report</h1>

		<div class="section">
			<h3>Design</h3>
			<p>
				In this assessment, we were asked to build an online phrasebook that obtains its material from its users. In other words, it should work as a wiki. The target audience is students at universities looking for a translation to a phrase. Hopefully some other user has provided this translation at an earlier point in time, letting the user get an appropriate response from the web application.
			</p>
			<p>
				This website decided to follow the design pattern that is common amongst online translators[1][2]. That is two boxes, each with a language menu above them. The left box represents the phrase the user is looking to have translated, in the source language, and the right box represents the translated phrase that the application will return, in the target language. An early prototype of the application before any functionality was added looked like the image below.
			</p>
			<img src="images/a.png" style="width:100%;">
			<p>
				As one can see from this image, elements in the page are kept to a bare minimum. The aforementioned boxes and language / topic options, as well as the website title on the top left and a simple navigation bar containing links to the other parts of the website on the top right are the only visible elements. This choice was made in order to allow the user to directly access the site&#39;s functionality, without having to go through any unnecessary steps, such as greeting pages and pop ups. A very important reason for the choice of such a common design is that there is a high likelihood that the student has encountered online translators before and is therefore familiar enough with it to simply start using it. Apart from keeping the design simple and providing sufficient blank space, the small amount of elements helps in making sure there is no redundant information presented to the user. Many different options and menus could be presented, but this application&#39;s philosophy lies in the idea that the user is here to do one task and it is expected of the site that that one task will be performed well.
			</p>
			<p>
				Complying with Nielsen&#39;s 80% rule, the content of the page (in our case, the part of the site that contains its functionality) accounts for more than 80% of the page, leaving the remaining small percentage to navigation. Only having a handful of elements also means fast response times for the user when moving between pages, thus making sure that their interest is maintained and most importantly, the action they are trying to achieve is not obstructed. Links never open in a new window, as users tend to find that confusing and makes the back button inoperable.
			</p>
			<p>
				Colors, a combination of dark blue and light grey, were deliberately kept discrete, in an effort to avoid provoking different cultural sensitivities, especially considering that international students are the primary target group. The simple contrast of the two colours also means that people with color deficiencies will not have a problem using the application. The visual part of the application is consistent throughout its pages, as are the terms used for the site&#39;s functionality, thus conforming to Nielsen&#39;s heuristic of consistency [3].
			</p>
		</div>

		<div class="section">
			<h3>Functionality</h3>
			<p>
				The user is provided with the following functionality by the application: The user can search for a phrase&#39;s translation, contribute to the phrasebook, request a phrase to be translated or add a missing topic or language to the wiki. If the user thinks the translation is incorrect, he can also follow a link to edit that particular translation.
			</p>
			<p>
				In the search page, the user can first choose the source language (the language of the phrase he is providing), the topic of the translation he is looking for, as well as the target language (the language he wants the phrase translated to). The left box in the design lets the user input some text, and given the parameters chosen in the drop-down menus, after clicking on the &ldquo;Search&rdquo; button, the application will look through its database for a match. If a translation is found, it is printed on the screen. Some text also appears., notifying the user that he can edit the translation if he thinks it is inadequate. Two buttons are also shown, allowing the user to mark a translation as spam or flag it as inappropriate. These buttons use AJAX in order to quickly access the database and add the translation to the appropriate table. They are explained in more depth in the security section.
			</p>
			<img src="images/b.png" style="width:100%;">
			<img src="images/c.png" style="width:100%;">
			<p>
				The search button calls a PHP function, that in turn uses a PDO to look through the databases and return the result. If no match was found, the function returns the phrase &ldquo;No translation was found.&rdquo;. If however, the phrase was obviously similar to one that exists in the database, it returns a suggestion of what the user might have meant. This is done via PHP&#39;s levenshtein function. This function calculates the amount of characters that have to be deleted, replaced or inserted, in order to transform a string into another string. In this implementation, a certain threshold of maximum characters that need to change was set to assess the similarity of a phrase to the ones already in the database. Every time a translation is not found, a button appears (via jQuery) in order to let the user add the translation for the input phrase.
			</p>
			<img src="images/d.png" style="width:100%;">
			<img src="images/e.png" style="width:100%;">
			<p>
				If the user clicks on the &ldquo;Add translation&rdquo; button, he is taken to a page with both boxes of the design being text areas for input. The left box, as well as the drop-down menus, carry the values from the previous page, sparing the user the action of retyping the original phrase of the translation they want to contribute and reselecting the languages and topic. After typing in the translation of the phrase in the right-hand side box, the phrase and its translation to the target language are added to the database. If they already exist, the user is presented with an appropriate message. If one clicks on the contribute link in the navigation bar, he is taken to the page described above, but with no fields already filled in or selected. A PHP function on the server side deals with inserting the new entry to the database.
			</p>
			<img src="images/f.png" style="width:100%;">
			<img src="images/g.png" style="width:100%;">
			<img src="images/h.png" style="width:100%;">
			<p>
				As mentioned already, when a user searches for a phrase and a translation was found, the opportunity is given to the user to edit this translation. If the user follows that link, he is taken to a page where he can access the text of the translation and modify it according to what the user thinks is best. On submission, PHP code on the server side updates the database to reflect the changes.
			</p>
			<img src="images/i.png" style="width:100%;">
			<img src="images/j.png" style="width:100%;">
			<p>
				Another function the user can perform on this application is request a phrase to be translated. On clicking the &ldquo;Request&rdquo; link on the navigation bar, the user is taken to a page where he can choose the source language and the topic, input the phrase he wants translated, as well as choose the language he wishes his phrase to be translated to. On submission, a PHP function on the server side inserts to the relevant table an entry with an empty translation. Hopefully at a future point some other user will contribute the translation for this phrase.
			</p>
			<img src="images/k.png" style="width:100%;">
			<p>
				The final functionality provided by the website is adding a topic or language to the phrasebook, that the user thinks is missing. It is highly unlikely that the developers have thought of all the topics that could be useful to students (especially when it comes to specialized subjects, such as computational chemistry or cognitive psychology, for example). It is thus important to let the users add topics as they see fit. Hopefully, the list of languages provided by the application covers every user&#39;s needs. But if that is not the case, the site lets the user add a new language. The user will need to provide a two-letter ISO code and the language&#39;s name. A link to the Wikipedia entry of ISO language codes is provided for reference. The addition of a new language is likely to be infrequent, so the administrator can check the validity of the entry when it occurs.
			</p>
			<img src="images/l.png" style="width:100%;">
		</div>

		<div class="section">
			<h3>Security</h3>
			<p>
				One of the most important aspects of web development today is security. This part of this report will discuss the various threats posed to the website and the extent to which measures are taken to guard against these threats in the implementation. Most threats to this particular site revolve around taking advantage of vulnerabilities of the input fields, as every single page of the site has them.
			</p>
			<p>
				A typical exploitation of the input fields is SQL injections. An SQL injection is the process by which a malicious user uses special characters in the input fields in order to manage to get access to the application&#39;s database. Once access to the database has been established, the attacker can get all information inside it, add and delete entries or even drop tables from it. This of course should be no means be allowed. The input fields of this application are sent to PHP functions on the server. These PHP functions use prepared statements via PDOs. Prepared statements have the important advantage over directly working with SQL statements that they separate the input from the query to be executed. This way, the input can only be treated as a parameter to the query and not as a query itself. This neutralizes the attack, making sure that whatever the input is, it is only added to the database as text and that the attacker never gets control of the database.
			</p>
			<p>
				Input is also validated in different ways to ensure that attackers cannot send their own malicious scripts to the server. Form validation happens in Javascript through a group of functions designed to run when the submit button is clicked. If Javascript is disabled, or if the attacker can find a way around it, the equivalent PHP functions run on the server again, before any further function is called. They stop execution of all functions if suspicious behaviour is detected. The validation consists of a blacklist of special characters. Whitelists (declaring the input characters that the system accepts) are preferable to blacklists (declaring the input characters the system does not accept). The reason for this is that it is hard for the developer to think of all possible characters that need to be banned and some unexpected character could go through. This site, however, is designed to support various languages and consequently various writing systems, making the idea of a whitelist hard to achieve. Not allowing characters such as the greater than (>), less than (<), double quote (&rdquo;) or ampersand (&) symbols helps the site avoid attacks such as code injection and cross-site scripting, since they are characteristically used to insert scripts into forms. Some characters (eg. semicolon, dot, comma) were intentionally omitted from the blacklist, as they are used in natural language in various languages and would unnecessarily limit the user&#39;s choices when it comes to using text in input fields. Attackers can also use numerical input to perform their attacks. As this site is a phrasebook, numbers are not expected input and are not permitted. In order to guard against buffer overflows, there is a limit to how many characters a user can type in each text field. [4]
			</p>
			<img src="images/m.png" style="width:100%;">
			<img src="images/n.png" style="width:100%;">
			<p>
				Coding errors from the server are never printed on the client side, as this could potentially give the attacker useful information about the server system. This information could in turn be used against the application to the attacker&#39;s advantage. This website does not use PHP sessions or cookies and does not have login functionality. There is therefore no security threat of an attacker stealing the user&#39;s cookie or obtaining their session information via cross-site scripting ot packet sniffing in order to steal their information. If sessions were used, it would be beneficial to the site&#39;s security to create a new session every time the user visits the site or every time the user logs in. PHP&#39;s less safe GET is used only on the client side and never to send information to the server. Every time a form is submitted, it is done via the safer POST functionality.
			</p>
			<p>
				Another important threat to security is spam. The approach to guard against this taken by the application is to use a bayesian filter to detect spam messages. The spam filter that was used was b8, written by Tobias Leupold [5]. The filter uses a &ldquo;naive bayes classifier&rdquo; to detect whether a given text is spam or not by looking at the words and the frequency that they appear in its training set (a labeled table in the database). The filter has two advantages. One is that it can stop humans from spamming the website, as well as bots that scan webpages looking for forms. The other is that it gives the opportunity to the user to manually mark an entry as spam. This helps the filter find omissions from its training set and helps it perform better in the future. The main disadvantage of the bayesian filter however, is that it needs a proper and preferably large training set of user input. This means that its performance in the early stages of the application&#39;s life will not necessarily be up to standards, but will improve over time, as it acquires more data to work with.
			</p>
			<img src="images/o.png" style="width:100%;">
			<p>
				An alternative approach for protecting against spam would be to use a &ldquo;honeypot&rdquo;. This is an additional input field that is inserted to the form. If its style attribute &ldquo;visibility&rdquo; is set to &ldquo;hidden&rdquo;, then it is invisible to the user. Bots however, have a harder time detecting hidden elements, as they just scan the code, and will fill in the input field. When validating the form, the developer can check if that field&#39;s value is empty. If it is not empty, a bot has been detected. It is important not to hide the element in the HTML tag, but to use CSS to hide the element. Many bots can detect HTML attributes and if they find that an item is hidden, they leave it alone. The disadvantage of the honeypot method  is that it can only detect automated bots and not manual attacks. Yet another method against spam would be to use CAPTCHA, a plug-in where the user needs to understand a piece of distorted text and type it in a text field in order to proceed. This method can be very successful in stopping bots, as they have great difficulty understanding blurred or distorted text. It is however, an extra step for the user and not good from a usability perspective, as the user needs to stop, think and type, breaking the flow of interaction. It is the developer&#39;s responsibility to detect bots and stop them, not the user&#39;s.
			</p>
			<p>
				Further issues the website needs to protect itself from are vandalism and edit wars. They are not a security threat exactly, but they are crucial to the phrasebook&#39;s integrity nonetheless. Similar to letting the user mark a translation as spam, one can also flag a translation as inappropriate. Some other user might have found an open platform to be an opportunity to post offensive material or text that most users would find is in bad taste. By flagging the posts as inappropriate, the moderator&#39;s attention can be drawn to them. They can decide whether the message is truly inappropriate and remove it if it is. Users that participate in vandalism could be identified through PHP sessions or cookies and blocked from the site altogether. The database could also be backed up in regular intervals, so that the administrator can revert the database to its pre-vandalism state.
			</p>
			<p>
				Edit wars can occur when two users disagree on what the best translation for a given phrase is. Since all users have the ability to edit translations, they could end up correcting each other indefinitely. The first step to be taken against this behaviour is to detect it. The system could keep track of the number of edits a certain phrase has underwent. If the number of edits within a certain time frame exceeds a predefined threshold, then the moderator can investigate the possibility of an edit war. The phrase could temporarily be set as non-editable, forcing the users to stop the war. This is however, only a temporary measure. The wiki could have a policy of warning the user that is repeatedly changing the translation and if they fail to comply, block them from using the system. 
			</p>
		</div>

		<div class="section">
			<h3>Usability</h3>
			<p>
				To assess the usability of this website, the Petrie and Power heuristics for interactive websites were considered. What follows is a short discussion of each of the heuristics in the context of the application implemented for this assessment, as well as a table showing the most important usability problems and their perceived severity.
			</p>
			<p class="category">Physical presentation</p>
			<p>
				<span class="heuristic">1. Make text and interactive elements large and clear enough :</span> The text in the navigation bar is large and clear with strong contrast (white on dark blue). The drop down menus have adequately large labels. The text in the menus could be slightly larger. The results of the translation are large enough, but the contrast of the text to the background color could be stronger. Text in input fields should be larger. It should also have some padding, giving it some space between the outline of the text field and the text, as it can become difficult for the user to read text that is unnecessarily cramped. 
			</p>
			<p>
				<span class="heuristic">2. Make page layout clear :</span> The design follows a conventional layout of online translators. This means that many users will recognize the layout and will be able to use the application straight away. This however, is only an assumption and if it fails or if the user simply is not familiar with the layout, one could potentially be confused about what to do. There is no direct instruction guiding the user, but only indirect hints, through the position of elements, such as the &ldquo;From&rdquo; and &ldquo;To&rdquo; labels or the search button. These however, are likely not sufficient. The organization of the material is reflected by the layout of the page and the material is easy to read.
			</p>
			<p>
				<span class="heuristic">3. Avoid short time-outs and display times :</span> There are no instances of elements timing out or disappearing. Translations, as well as feedback and error messages stay on the screen until the user decides to take another action. For example, when a user has given a numerical input as a search term, the appropriate error message will appear and will stay there until the user decides to change their input and search again. This is however, taking things to the other extreme. Messages (especially non-error ones) could time out after a reasonable amount of time. Error messages could disappear once the user starts editing their input.
			</p>
			<p>
				<span class="heuristic">4. Make key content and elements and changes to them salient :</span> All elements on the page are clearly visible. Drop-down menus are adequately spaced and properly labeled. The input fields have a different color from the other elements and are positioned in such a way that implies their functionality. The output areas&#39; functionality however is not entirely obvious until the function has been run at least once. The translation area in the search page, for example appears like an empty blue box. It is unclear to the user that this is where the search results will be displayed. Inadequate attention is drawn by the buttons that appear at various instances. The &ldquo;Add translation&rdquo; button that appears if the user searches for a translation but none was found, could potentially be missed by him. Users might also find most pages too similar to each other and be confused as to which page they are on at any given moment.
			</p>
			<p class="category">Content</p>
			<p>
				<span class="heuristic">5. Provide relevant and appropriate content :</span> There is a minimal amount of content in this web application, as most of the elements are interactive. The content that does exist is always kept short and relevant to the task at hand. Feedback and error handling are also always relevant to the function the user is trying to perform. The wording in some occasions could be improved to become clearer, as well as reflect a friendlier attitude towards the user.
			</p>
			<p>
				<span class="heuristic">6. Provide sufficient but not excessive content :</span> There is enough interactive elements for the user to complete their task, but the content is insufficient to guide them through the steps needed to perform the task they want to achieve. No help documentation is provided. The website makes the mistake of assuming the user already knows how to use it.
			</p>
			<p>
				<span class="heuristic">7. Provide clear terms, abbreviations, avoid jargon :</span> Almost no technical terms are used. In the rare occasion that they do (eg. when the word &ldquo;spam&rdquo; is used), it is terminology that is widely known and their function in the page does not hinder the user&#39;s goals. The only abbreviation used in the page is &ldquo;ISO&rdquo; in the &ldquo;Missing Topics & Languages&rdquo; page. But this is a link to an external site that explains the abbreviation for those that are interested.
			</p>
			<p class="category">Information Architecture</p>
			<p>
				<span class="heuristic">8. Provide clear, well-organized information structures :</span> The information on the page is clearly organized into (i) navigation, (ii) the information on the phrase to be translated and (iii) the information of the phrase&#39;s translation. Visually these parts are separated by color, indicating their semantic grouping. The &ldquo;Missing Topics & Languages&rdquo; page is also separated into (i) the navigation bar, (ii) a box containing the needed instructions and input fields for adding a new topic to the phrasebook, as well as (iii) an equivalent box for the functionality of adding a new language. Despite the information being organized, problems could arise concerning how clear this structure is to the user. Better descriptions of the structure is perhaps needed.
			</p>
			<p class="category">Interactivity</p>
			<p>
				<span class="heuristic">9. How and why :</span>  The application is lacking in this respect. No explanations of how the interactivity works are given to the user whatsoever. The user is expected to know how drop-down menus work and he is also expected to recognize text areas. Small changes, like text in the text fields explaining to the user the element or instructions that appear on hovering the mouse above elements could improve the website experience significantly.
			</p>
			<p>
				<span class="heuristic">10. Clear labels and instructions :</span> From and To labels are not necessarily clear enough. The user might not understand that they concern the choice of language for their input phrase and translation. No instructions are given to the user on where to type the phrase they want translated, or the steps they need to follow in order to achieve their purpose. No information is given on what is considered valid input, so unless the user has already typed something and gotten an error message, it is unclear to them what the expected input format is. Instructions are better in the &ldquo;Missing topics and languages&rdquo; section of the site. The words used for some buttons might not necessarily convey the same clear meaning to all users. &ldquo;Add translation&rdquo; is something a user might possibly find vague. The same problem exists in the navigation bar. &ldquo;Search&rdquo; is clear enough, but &ldquo;Contribute&rdquo; and &ldquo;Request&rdquo; might be misleading to the user as to what the functionality they represent actually is. &ldquo;Missing Topics & Languages&ldquo; could lead the user to think that he will be taken to a list of missing topics and languages, which is not the case.  Words that represent the meaning of these elements better should be chosen.
			</p>
			<p>
				<span class="heuristic">11. Avoid duplication / excessive effort by users :</span> Measures were taken so that when the user moves between pages, some relevant info is transferred from one page to another. For example, when the user searches for a translation and then they decide they want to edit the translation, the chosen languages, topic and the phrases in both languages are transferred to the edit page, so that the user does not have to provide that input again. The same happens when the user clicks on the add translation button on the search page. A problem with extra effort comes from the fact that there is no autocomplete functionality in the input fields. If the user has simply misspelled something, they have to wait for the suggestion of a similar phrase from the database to understand that their phrase had a mistake (if a similar phrase exists in the database).
			</p>
			<p>
				<span class="heuristic">12. Make input formats clear and easy :</span> As mentioned before in this report, the user is not informed of what is considered valid input by the application. He would have to discover that via trial and error. Plain text as input for a phrasebook could be considered common sense, but a user might not understand why some special metacharacters (eg. >) or numbers are forbidden from input. Another case where the input format is unclear to the user is the ISO code entry input field in the &ldquo;Missing Topics & Languages&rdquo; part of the website. The user has to either follow the link, or know beforehand that an ISO code is a two-letter code for a language.
			</p>
			<p>
				<span class="heuristic">13. Provide feedback on user actions and system progress :</span> This is a fairly simple implementation and subsequently fast, thus minimizing the possibility of the user waiting for anything. In the unlikely event this happens however, there is no feedback to the user that the function is in progress. Feedback is provided for other actions. For example, if the user edits a phrase from the phrasebook, the message &ldquo;You have successfully edited this entry&rdquo; is printed on the screen. Appropriate messages are provided for all different actions. When the translation of a phrase is not found and the user had selected a specific topic, if such a phrase exists in the phrasebook, the user gets a suggestion that they could try searching again with the topic set to &ldquo;Any&rdquo;.
			</p>
			<p>
				<span class="heuristic">14. Make the sequence of interaction logical :</span> The sequence of interaction is, for the most part, logical. Language options exist above the relevant input and output fields. Buttons exist in positions that make sense for their function. For example, the search button exists underneath the phrase that will be searched. In the page where the user edits a result, the submit button exists underneath the phrase that will be edited. The buttons exist at the bottom right of their respective input fields, reflecting the direction most western readers would read in. A problem exists in the &ldquo;Search&ldquo; page, where the search button appears visually before the output language option, as the output language is part of the parameters of searching.
			</p>
			<p>
				<span class="heuristic">15. Provide a logical and complete set of options :</span> The set of options given to the user is logical and sufficient to cover the requirements of the project. The user can search, edit, add or request a given phrase for translation. The options given to the user are far from complete, but it is this application&#39;s philosophy that only the essentials are given to the user in order to avoid cluttering the interface. This could be harmful to some users&#39; experience, as they might be looking for more advanced options when using the application.
			</p>
			<p>
				<span class="heuristic">16. Follow conventions for interaction :</span> For the most part, conventions for interactions are kept. The navigation bar is at the top right, where the user might expect it to be. A problem occurs when the user tries to press tab to move between interactive elements in the search page. The &ldquo;Search&rdquo; button is chosen before the output language, even though the output language is a parameter of search. That would force the user that is using a keyboard to either shift-tab back to the search button or reach for the mouse to complete the action. Both options are sub-optimal.
			</p>
			<p>
				<span class="heuristic">17. Provide the interactive functionality users will need and expect :</span> The site provides the necessary interactive functionality the user needs to complete the different actions of the requirements. The lack of autocomplete while typing hinders the user from getting suggestions from the system when he is unsure of what the input should be. The user might also expect to see a list of phrases that have not been translated yet when clicking on the &ldquo;Contribute&rdquo; link of the navigation menu, if their goal is to improve the content of the phrasebook. Appropriate links and buttons are provided at the points when the user might want to edit a translation, mark is it as spam or flag it as inappropriate.
			</p>
			<p>
				<span class="heuristic">18. Indicate if links go to an external site or to another web page :</span> There is only one link that takes the user to another web page. This is the link on the &ldquo;Missing Topics & Languages&rdquo; page that takes the user to the Wikipedia entry of ISO codes. The fact that the link takes the user to an external website is in no way not indicated, which is problematic.
			</p>
			<p>
				<span class="heuristic">19. Interactive and non-interactive elements should be clearly distinguished :</span> There is visual distinction between interactive and non-interactive contents. Buttons and drop-down menus have a distinct visual style from the rest of the application. The color contrast between them and the rest of the application could be stronger, but their color is also dependent on the browser, as the html elements that represent them are default HTML5 elements (eg. the button tag). Areas that include text fields are different from areas where only output from the system will be printed. It is not entirely obvious to the user that the navigation bar is interactive until they hover over the links on it (which will slightly alter the font to bold).
			</p>
			<p>
				<span class="heuristic">20. Group interactive elements clearly and logically :</span> Elements are grouped logically together. The links to other pages are grouped together in the navigation bar. All information relevant to the source language phrase is grouped in one box and displayed on the left hand side. All information related to the translation of the phrase are grouped in the right hand side box. The problem that could arise is that the user might not understand the meaning of this separation. Furthermore, it is probably not intuitive to everyone that the &ldquo;Topic&rdquo; drop-down menu should be above the source language phrase. Some users might find it more reasonable for it to be separate from the languages. In the missing page, missing topic and missing language are similarly separated in boxes.
			</p>
			<p>
				<span class="heuristic">21. Provide informative error messages and error recovery :</span> Error messages are displayed if something goes wrong with form submissions. Numerical input, empty form elements, special characters and trying to have a phrase translated in its own language all give distinct informative messages on why the input is not valid. A problem is that these errors are checked by the code in turn. That means that if several errors occur, the user will only get an error message relating to one of them. This could lead the user to having several attempts of submitting the form before they manage to understand what is considered valid input.
			</p>
			<p></br></br>Below is presented the table of problems that was created as part of this heuristic evaluation.</p>
			<img src="images/p.png" style="width:60%;">
		</div>

		<div class="section">
			<h3>Accessibility</h3>
			<p>
				To assess the extent to which this website is accessible, the guidelines given by the WCAG 2.0 Guide were followed. The goal was to achieve Level A accessibility, so that people with special needs can also use this website without a problem.
			</p>
			<p>
				There is no media on this website such as audio, video, images or other forms of multimedia. There are also no elements whose appearance on the site has a time limit and no elements that flash. Measures that can be taken to make these elements more accessible are not applicable to this specific implementation. 
			</p>
			<p>
				Specific tools were used to assess certain aspects of accessibility. The HTML source code of all pages was validated in the w3c validator [6] and no significant errors were found. The color contrast of the fonts and their background was checked at WebAim [7] and all combinations used by the website received at least a WCAG AA level pass. The Web Developer Toolbar plugin [8] was also installed on Firefox and no CSS or Javascript errors were found. Through A function of this toolbar, CSS was disabled and it was manually checked that the flow of information, navigation elements, form elements, links and instructions is logical and intuitive. As mentioned already in the usability section (heuristic 16), there is one problem. In the search page, the drop-down menu to choose the output language is after the search button. This ordering of elements is counterintuitive and also creates a problem for keyboard users, by forcing them to go back. It is the only error in logic and ordering detected in the website however, and all other elements are ordered as expected.
			</p>
			<img src="images/q.png" style="width:100%;">
			<p>
				These automated checks were not enough to check the levels of accessibility of the application, so further, manual tests needed to be undertaken. 
			</p>
			<p>
				As far as forms are concerned, it was made sure that all form buttons have a descriptive value attribute. All drop-down menus have labels with a for attribute, associating the labels with the menus. Text fields that have no labels have title attributes that describe what they are, unless they have labels, in which case the same for attribute as with the drop-down menus was used. If these input fields have restrictions that apply to what their content could be, those restrictions are mentioned in the title attribute. If errors occur, the error messages appear underneath or next to the elements that had the erroneous input. The messages are informative and help the user quickly correct their input. There are input text areas in forms that don&#39;t have sufficient instructions and labels. These issues are discussed in detail in the usability section.
			</p>
			<p>
				Semantic HTML5 markup has been used in this website. Header and nav tags have been used for the page title / logo and the navigation menu respectively. The ul tag has been used for the list of list in the navigation bar. In the &ldquo;Missing topics and Languages&rdquo; page, the ISO abbreviation has been surrounded with the abbr tag, so that the user can easily find what the abbreviation stands for. The language of the document is declared in the html tag of all pages. The page&#39;s title is descriptive and informative. So are the names of the links. When the names of the links are not sufficient, their title attribute provides a more detailed description of what they do. The issue of the words chosen to describe links and button names is discussed in further detail in the usability section.
			</p>
			<p>
				No instructions rely on visual or audio elements. Colors are never used as the sole indicator of conveying content to the user. The links that are recognized by their different color from the rest of the text are also underlined, giving the user an alternative way to recognize the extra functionality. No keyboard locks exist, and all functionality of the website is available to the user that has to use only the keyboard. There is no element or trigger to an element that will cause unexpected substantial changes to the site, disorienting the user (such as pop-ups).
			</p>
			<p>
				No link is provided to skip navigation, even though the header and navigation menu are repeated in all pages. This is on purpose, as the menu is sufficiently small to not be considered a block to the user trying to get to each page&#39;s main content. [9]
			</p>
			<p>
				If the user has for some reason javascript disabled on their browser, the site will still function properly. The main function of the jQuery functions in the implementation is error handling, but php functions on the server can take care of errors if javascript fails to run. The only functionality that is not available to the user with Javascript disabled is marking translations as spam or flagging them as inappropriate.
			</p>
		</div>

		<div class="section">
			<h3>Code Documentation</h3>
			<p>
				The PHP and Javascript code is organized into functions to facilitate the reusability of components. Sufficient, but not excessive amounts of comments exist in the code to help with the application&#39;s maintainability. The HTML code is organized and indented to make reading it easier for an interested stakeholder and follows the flow of the elements as they appear in the webpage. The amount of files was kept to a reasonable number, so that a future reader of the code does not have to navigate through an excessive and complicated structure to find the component the reader is interested in.
			</p>
		</div>

		<div class="section">
			<h3>References</h3>
			<p>
				[1] <a href="http://translate.google.com/">Google Translate</a></br>
				[2] <a href="http://www.bing.com/translator">Bing Translator</a></br>
				[3] <a href="http://www.nngroup.com/articles/ten-usability-heuristics/">Ten usability heuristics for user interface design</a></br>
				[4] <a href="http://www.amazon.co.uk/Innocent-Code-Security-Wake-up-Programmers/dp/0470857447">Innocent code</a></br>
				[5] <a href="http://nasauber.de/opensource/b8/">b8</a></br>
				[6] <a href="http://validator.w3.org/">W3C Validator</a></br>
				[7] <a href="http://webaim.org/resources/contrastchecker/">WebAIM Color Contrast Checker</a></br>
				[8] <a href="https://addons.mozilla.org/en-US/firefox/addon/web-developer/">Web Developer Toolbar</a></br>
				[9] <a href="http://www.w3.org/TR/UNDERSTANDING-WCAG20/navigation-mechanisms-skip.html">Bypass Blocks</a></br>
			</p>
		</div>
	</div>



</body>

</html>